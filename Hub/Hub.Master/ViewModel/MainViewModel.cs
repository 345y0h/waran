﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WARAN.Hub.Master.Model;
using Windows.UI.Xaml;

namespace WARAN.Hub.Master.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// MyModules list
        /// </summary>
        private ObservableCollection<Model.MyModule> myModules;
        public ObservableCollection<Model.MyModule> MyModules
        {
            get
            {
                return myModules;
            }
            set
            {
                Set(() => MyModules, ref myModules, value);
            }
        }

        /// <summary>
        /// List of all modules
        /// </summary>
        private ObservableCollection<Model.Modules.BaseModule> modules;
        public ObservableCollection<Model.Modules.BaseModule> Modules
        {
            get
            {
                return modules;
            }
            set
            {
                Set(() => Modules, ref modules, value);
            }
        }

        /// <summary>
        /// Property to toggle add new module control
        /// </summary>
        private Visibility showAddNew = Visibility.Collapsed;
        public Visibility ShowAddNew
        {
            get
            {
                return showAddNew;
            }
            set
            {
                Set(() => ShowAddNew, ref showAddNew, value);
            }
        }
        
        private RelayCommand toggleAddNewCommand;
        public RelayCommand ToggleAddNewCommand
        {
            get
            {
                return toggleAddNewCommand ?? (toggleAddNewCommand = new RelayCommand(() => {
                    ToggleAddNew();
                }));
            }
        }

        /// <summary>
        /// Add a new module to the hub
        /// </summary>
        /// <typeparam name="T">Any module type inheriting from BaseModule</typeparam>
        /// <param name="Data">New module that has to be added to the hub</param>
        public async void AddModule<T>(T Data)
        {
            Model.Modules.BaseModule module = null;
            Model.Modules.ModuleType moduleType = Model.Modules.ModuleType.None;
            //Adding module to table based on the type
            if(typeof(T) == typeof(Model.Modules.PlantWaterer))
            {
                module = DB.Save<Model.Modules.PlantWaterer>((Model.Modules.PlantWaterer)(object)Data); //Save module entry to table
                moduleType = Model.Modules.ModuleType.PlantWaterer;
            }
            else if(typeof(T) == typeof(Model.Modules.TemperatureMonitor))
            {
                module = DB.Save<Model.Modules.TemperatureMonitor>((Model.Modules.TemperatureMonitor)(object)Data);
                moduleType = Model.Modules.ModuleType.TemperatureMonitor;
            }
            //Adding a reference mymodule list
            Model.MyModule myModule = new Model.MyModule() { AddedOn = DateTime.UtcNow, ModuleId = module.Id, ModuleType = moduleType, Id = Guid.NewGuid().ToString() };
            myModule = DB.Save<Model.MyModule>(myModule);
            Modules.Add(module);
            MyModules.Add(myModule);
            //Saving module and mymodule in Azure Mobile Service
            await Cloud.Add<T>(Data);
            await Cloud.Add<Model.MyModule>(myModule);
        }

        /// <summary>
        /// Updating a module in the DB and cloud
        /// </summary>
        /// <typeparam name="T">Any module type inheriting from BaseModule</typeparam>
        /// <param name="Data">Updatd module data</param>
        public async void SaveModule<T>(T Data)
        {
            DB.Save<T>(Data);
            await Cloud.Save<T>(Data);
        }

        /// <summary>
        /// Deleting a module from db and also the mymodule reference
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Data"></param>
        public void DeleteModule<T>(T Data)
        {
            Model.Modules.BaseModule module = (Model.Modules.BaseModule)(object)Data;
            var myModule = MyModules.Where(p => p.ModuleId == module.Id).FirstOrDefault();
            DB.Delete<T>(Data);
            DB.Delete<Model.MyModule>(myModule);
            MyModules.Remove(myModule);
            Cloud.Delete<T>(Data);
            Cloud.Delete<Model.MyModule>(myModule);
        }

        private void ToggleAddNew()
        {
            ShowAddNew = ShowAddNew == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
        }

        public MainViewModel()
        {
            MyModules = new ObservableCollection<MyModule>(DB.GetMyModules());
            Modules = new ObservableCollection<Model.Modules.BaseModule>(DB.GetModules());
        }

    }
}
