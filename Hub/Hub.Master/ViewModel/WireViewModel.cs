﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;

namespace WARAN.Hub.Master.ViewModel
{
    public class WireViewModel
    {

        private I2cDevice Device;

        public bool InProgress = false;

        /// <summary>
        /// RPI2 sends a data to Arduino Uno via I2C which in turn will send teh data to the module via RF
        /// The module will send response back to Arduino Uno via RF which will then send the data back to RPI2 via I2C
        /// </summary>
        /// <param name="Address">Address of the module</param>
        /// <param name="Command">The command that should be sent to the module</param>
        /// <param name="CommandData">Data that has to be sent to module</param>
        /// <returns></returns>
        public String RequestData(String Address, String Command, String CommandData = null)
        {
            InProgress = true;
            try
            {
                //Build a combined string in the format <Address>$<Command> or <Address>$<Command>$<CommandData>
                String Data = Address + "$" + Command;
                if (!String.IsNullOrEmpty(CommandData))
                {
                    Data += "$" + CommandData;
                }
                
                byte[] ReadBuf = new byte[Data.Length];
                Device.Write(Encoding.UTF8.GetBytes(Data)); //Wite
                bool timeout = false;
                DateTime StartTime = DateTime.Now;
                do //Wait until a proper data is received from Arduino Uno
                {
                    if (DateTime.Now.Subtract(StartTime).TotalSeconds >= 2) //Wait a maximum of 2 seconds else mark the response as timed out
                    {
                        timeout = true;
                        break;
                    }
                    try
                    {
                        var data = Device.ReadPartial(ReadBuf);
                    }
                    catch (Exception ex) { Debug.WriteLine(ex); }
                } while (Array.FindLastIndex(ReadBuf, b => b != 255) < 5); //A valid response should have atleast 5 characters in it
                InProgress = false;
                return timeout ? "TIMEOUT" : Encoding.UTF8.GetString(ReadBuf, 0, Array.FindLastIndex(ReadBuf, b => b != 255) + 1);
            }
            catch(Exception ex)
            {
                InProgress = false;
                Debug.WriteLine(ex);
                return "FAILED"; //In case of any exception return response as failed
            }
            
        }

        public WireViewModel()
        {
            I2cConnectionSettings settings = new I2cConnectionSettings(0x40); // Arduino Uno slave address
            settings.BusSpeed = I2cBusSpeed.StandardMode;
            string aqs = I2cDevice.GetDeviceSelector("I2C1");
            DeviceInformationCollection dis = null;
            Task.Factory.StartNew(new Action(async () =>
            {
                dis = await DeviceInformation.FindAllAsync(aqs);
                Device = await I2cDevice.FromIdAsync(dis[0].Id, settings);
            }));
        }

    }
}
