﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WARAN.Hub.Master.Views
{
    /// <summary>
    /// Page to display the list of modules added in the Hub. This is page that is loaded initially
    /// </summary>
    public sealed partial class ModuleListPage : Page
    {
        public ModuleListPage()
        {
            this.InitializeComponent();
            this.Loaded += ModuleListPage_Loaded;
        }

        private void ModuleListPage_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
