﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using WARAN.Hub.Master.ViewModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace WARAN.Hub.Master.Views.Controls.Modules
{
    public sealed partial class TemperatureMonitor : UserControl
    {

        public Model.Modules.TemperatureMonitor CurrentData = null;

        ViewModelLocator locator = null;

        public const String COMMAND_READ_TEMPERATURE = "READ_TEMPERATURE";
        public const String COMMAND_START_THERMO = "START_THERMO";
        public const String COMMAND_STOP_THERMO = "STOP_THERMO";
        public const String TIMEOUT = "TIMEOUT";
        public const String FAILED = "FAILED";

        public Timer timer;

        public TemperatureMonitor()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Constructor called from module container with module data passed to this
        /// </summary>
        /// <param name="Data"></param>
        public TemperatureMonitor(Model.Modules.TemperatureMonitor Data)
        {
            this.DataContext = this.CurrentData = Data; //Setting the module data as current data context
            this.InitializeComponent();
            this.Loaded += TemperatureMonitor_Loaded;
            Model.Cloud.CloudChanged += Cloud_CloudChanged; //Handing cloudchanged event triggered by pubnub
            timer = new Timer(TimerCallback, null, 1000, 600000); //Initiating the timer to check the temperature every 10 minutes
        }

        private async void TimerCallback(object state)
        {
            await this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                ReadTemperature(); //Send the read request every 25 minutes
                Save();
            });
        }

        /// <summary>
        /// Command received from mobile app
        /// </summary>
        private void Cloud_CloudChanged(object sender, Model.Cloud.CloudEventArgs e)
        {
            //Check if the command is sent to current module
            if (e.Data.Address == CurrentData.Address)
            {
                //Check type of command
                switch (e.Data.Command)
                {
                    case COMMAND_READ_TEMPERATURE:
                        ReadTemperature();
                        break;
                    case COMMAND_START_THERMO:
                        StartThermo();
                        break;
                    case COMMAND_STOP_THERMO:
                        StopThermo();
                        break;
                }
                Save();
            }
        }

        private void TemperatureMonitor_Loaded(object sender, RoutedEventArgs e)
        {
            locator = App.Current.Resources["ViewModelLocator"] as ViewModelLocator; //Get referecne to the view model locator
        }

        //Read data from module
        public void ReadTemperature()
        {
            String Response = RequestData(CurrentData.Address, COMMAND_READ_TEMPERATURE); //Sends the command COMMAND_READ_WATER_LEVEL to the address CurrentData.Address and gets back the response
            if (Response != TIMEOUT && Response != FAILED)
            {
                UpdateCurrentData(Response);
            }
        }

        //Start the device in module
        public void StartThermo()
        {
            String Response = RequestData(CurrentData.Address, COMMAND_START_THERMO);
            if (Response != TIMEOUT && Response != FAILED)
            {
                UpdateCurrentData(Response);
            }
        }

        //Stop the device in module
        public void StopThermo()
        {
            String Response = RequestData(CurrentData.Address, COMMAND_STOP_THERMO);
            if (Response != TIMEOUT && Response != FAILED)
            {
                UpdateCurrentData(Response);
            }
        }

        /// <summary>
        /// Manually read data from module
        /// </summary>
        private void Sync_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ReadTemperature();
        }

        /// <summary>
        /// Deleting/Removing the current module from the hub
        /// </summary>
        private void Delete_Tapped(object sender, TappedRoutedEventArgs e)
        {
            locator.Main.DeleteModule<Model.Modules.TemperatureMonitor>(CurrentData);
            timer.Dispose();
        }

        public void Save()
        {
            locator.Main.SaveModule<Model.Modules.TemperatureMonitor>(CurrentData); //Update the current state of the module in local DB and cloud
        }

        /// <summary>
        /// Switch ON/OFF the device.
        /// </summary>
        private void Device_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (CurrentData.ThermoRunning)
            {
                StopThermo();
            }
            else
            {
                StartThermo();
            }
        }

        private void UpdateCurrentData(String Data)
        {
            var task = this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                //The format of the response got from plant waterer would be like
                //<Temperature>|<Humidity>|<FeelsLike>|<ThermoRunning>
                //Eg. 32|67|34|1
                //For ThermoRunning the boolean is sent as "0" and "1"
                String[] splits = Data.Split('|');
                if (splits.Length == 5)
                {
                    CurrentData.Temperature = Double.Parse(splits[0]);
                    CurrentData.Humidity = Double.Parse(splits[1]);
                    CurrentData.FeelsLike = Double.Parse(splits[2]);
                    CurrentData.ThermoRunning = (splits[3] == "1");
                }
                Save();
            });
        }

        public string RequestData(String Address, String Command)
        {
            if (!locator.Wire.InProgress)
            {
                return locator.Wire.RequestData(CurrentData.Address, Command);
            }
            else
            {
                return FAILED;
            }
        }

    }
}
