﻿using GalaSoft.MvvmLight.Threading;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using WARAN.Hub.Master.Common.Utils;
using WARAN.Hub.Master.ViewModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace WARAN.Hub.Master.Views.Controls.Modules
{
    public sealed partial class PlantWaterer : UserControl
    {

        public Model.Modules.PlantWaterer CurrentData = null;

        ViewModelLocator locator = null;

        public const String COMMAND_READ_WATER_LEVEL = "READ_WATER_LEVEL";
        public const String COMMAND_START_PUMP = "START_PUMP";
        public const String COMMAND_STOP_PUMP = "STOP_PUMP";
        public const String COMMAND_ENABLE_AUTO_WATER = "ENABLE_AUTO_WATER";
        public const String COMMAND_DISABLE_AUTO_WATER = "DISABLE_AUTO_WATER";

        public const String TIMEOUT = "TIMEOUT";
        public const String FAILED = "FAILED";

        public Timer timer;

        public PlantWaterer()
        {
           this.InitializeComponent();
        }

        /// <summary>
        /// Constructor called from module container with module data passed to this
        /// </summary>
        /// <param name="Data"></param>
        public PlantWaterer(Model.Modules.PlantWaterer Data)
        {
            this.DataContext = this.CurrentData = Data; //Setting the module data as current data context
            this.InitializeComponent();
            this.Loaded += PlantWaterer_Loaded;
            Model.Cloud.CloudChanged += Cloud_CloudChanged; //Handing cloudchanged event triggered by pubnub
            timer = new Timer(TimerCallback, null, 2000, 1500000); //Initiating the timer to check the water level every 25 minutes
        }

        private async void TimerCallback(object state)
        {
            await this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                ReadWaterLevel(); //Send the read request every 25 minutes
                Save();
            });
        }
        /// <summary>
        /// Command received from mobile app
        /// </summary>
        private void Cloud_CloudChanged(object sender, Model.Cloud.CloudEventArgs e)
        {
            //Check if the command is sent to current module
            if(e.Data.Address == CurrentData.Address)
            {
                //Check type of command
                switch (e.Data.Command)
                {
                    case COMMAND_READ_WATER_LEVEL:
                        ReadWaterLevel();
                        break;
                    case COMMAND_START_PUMP:
                        StartPump();
                        break;
                    case COMMAND_STOP_PUMP:
                        StopPump();
                        break;
                    case COMMAND_ENABLE_AUTO_WATER:
                        EnableAutoWater();
                        break;
                    case COMMAND_DISABLE_AUTO_WATER:
                        DisableAutoWater();
                        break;
                }
            }
        }

        private void PlantWaterer_Loaded(object sender, RoutedEventArgs e)
        {
            locator = App.Current.Resources["ViewModelLocator"] as ViewModelLocator; //Get referecne to the view model locator
        }

        /// <summary>
        /// Deleting/Removing the current module from the hub
        /// </summary>
        private void Delete_Tapped(object sender, TappedRoutedEventArgs e)
        {
            locator.Main.DeleteModule<Model.Modules.PlantWaterer>(CurrentData);
            timer.Dispose();
        }

        /// <summary>
        /// Manually read data from module
        /// </summary>
        private void Sync_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ReadWaterLevel();
        }

        /// <summary>
        /// Switch ON/OFF the pump.
        /// </summary>
        private void Pump_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (CurrentData.PumpRunning)
            {
                StopPump();
            }
            else
            {
                StartPump();
            }
        }

        public void Save()
        {
            locator.Main.SaveModule<Model.Modules.PlantWaterer>(CurrentData); //Update the current state of the module in local DB and cloud
        }

        //Read the water level from module
        public void ReadWaterLevel()
        {
            String Response = RequestData(CurrentData.Address, COMMAND_READ_WATER_LEVEL); //Sends the command COMMAND_READ_WATER_LEVEL to the address CurrentData.Address and gets back the response
            if (Response != TIMEOUT && Response != FAILED)
            {
                UpdateCurrentData(Response);
            }
        }


        //Start the pump in the module
        public void StartPump()
        {
            String Response = RequestData(CurrentData.Address, COMMAND_START_PUMP);
            if (Response != TIMEOUT && Response != FAILED)
            {
                UpdateCurrentData(Response);
            }
        }

        //Stop the pump in the module
        public void StopPump()
        {
            String Response = RequestData(CurrentData.Address, COMMAND_STOP_PUMP);
            if (Response != TIMEOUT && Response != FAILED)
            {
                UpdateCurrentData(Response);
            }
        }

        //Enable autowatering in the module
        public void EnableAutoWater()
        {
            String Response = RequestData(CurrentData.Address, COMMAND_ENABLE_AUTO_WATER);
            if (Response != TIMEOUT && Response != FAILED)
            {
                UpdateCurrentData(Response);
            }
        }

        //Disable autowatering in the module
        public void DisableAutoWater()
        {
            String Response = RequestData(CurrentData.Address, COMMAND_DISABLE_AUTO_WATER);
            if (Response != TIMEOUT && Response != FAILED)
            {
                UpdateCurrentData(Response);
            }
        }

        private void UpdateCurrentData(String Data)
        {
            var task = this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                //The format of the response got from plant waterer would be like
                //<WaterLevel>|<Temperature>|<Humidity>|<PumpRunning>|<AutoWater>
                //Eg. 92|32|67|1|0
                //For PumRunning and AutoWater the boolean is sent as "0" and "1"
                String[] splits = Data.Split('|');
                if (splits.Length == 5)
                {
                    CurrentData.WaterLevel = Int32.Parse(splits[0]);
                    CurrentData.Temperature = Double.Parse(splits[1]);
                    CurrentData.Humidity = Double.Parse(splits[2]);
                    CurrentData.PumpRunning = (splits[3] == "1");
                    CurrentData.AutoWater = (splits[4] == "1");
                }
                Save();
            });
        }

        public string RequestData(String Address, String Command)
        {
            if (!locator.Wire.InProgress)
            {
                return locator.Wire.RequestData(CurrentData.Address, Command);
            }
            else
            {
                return FAILED;
            }
        }

    }
}
