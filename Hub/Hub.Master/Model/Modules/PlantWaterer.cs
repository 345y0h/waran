﻿using Newtonsoft.Json;
using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WARAN.Hub.Master.Model.Modules
{
    [DataContract(Name = "plantwaterer")]
    public class PlantWaterer : BaseModule
    {
        /// <summary>
        /// The water level in the soil
        /// </summary>
        private int waterLevel = 0;
        [JsonProperty("waterLevel")]
        public int WaterLevel
        {
            get {
                return waterLevel;
            }
            set
            {
                Set(() => WaterLevel, ref waterLevel, value);
            }
        }

        /// <summary>
        /// The temperature of the surrounding environment
        /// </summary>
        private double temperature = 0;
        [JsonProperty("temperature")]
        public double Temperature
        {
            get
            {
                return temperature;
            }
            set
            {
                Set(() => Temperature, ref temperature, value);
            }
        }

        /// <summary>
        /// Temperature in Fahrenheit
        /// </summary>
        [Ignore]
        public double TemperatureF
        {
            get
            {
                return Common.Utils.Temperature.CelsiusToFahrenheit(Temperature);
            }
        }

        /// <summary>
        /// Humidity level of the surrounding environment
        /// </summary>
        private double humidity = 0;
        [JsonProperty("humidity")]
        public double Humidity
        {
            get
            {
                return humidity;
            }
            set
            {
                Set(() => Humidity, ref humidity, value);
            }
        }

        /// <summary>
        /// To enable the plant waterer to automatically start the pump when the water level goes below the min threshold 
        /// and stop the pump when it reaches the max threshold
        /// </summary>
        private bool autoWater = false;
        [JsonProperty("autoWater")]
        public bool AutoWater
        {
            get
            {
                return autoWater;
            }
            set
            {
                Set(() => AutoWater, ref autoWater, value);
            }
        }

        /// <summary>
        /// To identify whether the pump is currently running or not
        /// </summary>
        private bool pumpRunning = false;
        [JsonProperty("pumpRunning")]
        public bool PumpRunning
        {
            get
            {
                return pumpRunning;
            }
            set
            {
                Set(() => PumpRunning, ref pumpRunning, value);
            }
        }

        public PlantWaterer(): base(ModuleType.PlantWaterer)
        {
            
        }
        public PlantWaterer(String Id, String Name): base(Id, Name, ModuleType.PlantWaterer)
        {
        }
    }
}
