﻿using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WARAN.Hub.Master.Model.Modules
{
    public class BaseModule : ObservableObject
    {
        /// <summary>
        /// Id of the object taht will be the primary in sqlite db and also in azure mobile service
        /// </summary>
        private String id = String.Empty;
        [PrimaryKey]
        [JsonProperty("id")]
        public String Id
        {
            get
            {
                return id;
            }
            set
            {
                Set(() => Id, ref id, value);
            }
        }

        /// <summary>
        /// Name given to that particular module eg. Home Garden, Master Bedroom
        /// </summary>
        private String name = String.Empty;
        [JsonProperty("name")]
        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                Set(() => Name, ref name, value);
            }
        }

        /// <summary>
        /// Address will be the RF address given to the particular module. This will hard coded in the arduino code of each module
        /// </summary>
        private String address = String.Empty;
        [JsonProperty("address")]
        public String Address
        {
            get
            {
                return address;
            }
            set
            {
                Set(() => Address, ref address, value);
            }
        }

        /// <summary>
        /// Defines what type of module it is
        /// </summary>
        private ModuleType type = ModuleType.None;
        [JsonProperty("type")]
        public ModuleType Type
        {
            get
            {
                return type;
            }
            set
            {
                Set(() => Type, ref type, value);
            }
        }

        /// <summary>
        /// Time at which the module is added to the hub
        /// </summary>
        private DateTime addedOn = DateTime.MinValue;
        [JsonProperty("addedOn")]
        public DateTime AddedOn
        {
            get
            {
                return addedOn;
            }
            set
            {
                Set(() => AddedOn, ref addedOn, value);
            }
        }

        public BaseModule(ModuleType Type)
        {
            this.Type = Type;
        }

        public BaseModule(String Id, String Name, ModuleType Type)
        {
            this.Id = Id;
            this.Name = Name;
            this.Type = Type;
            this.AddedOn = DateTime.Now;
        }
    }
}
