﻿using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WARAN.Hub.Master.Model.Modules;

namespace WARAN.Hub.Master.Model
{
    /// <summary>
    /// MyModule is list which contains reference to all the modules added to the hub.
    /// Since each module has its own data structure they all cannot be added to a single table.
    /// So we add modules to tables based on their type and add a refernce to it in MyModule table.
    /// </summary>
    [DataContract(Name = "mymodules")]
    public class MyModule : ObservableObject
    {
        /// <summary>
        /// Id of the current entry
        /// </summary>
        private String id;
        [PrimaryKey]
        [JsonProperty("id")]
        public String Id
        {
            get
            {
                return id;
            }
            set
            {
                Set(() => Id, ref id, value);
            }
        }

        /// <summary>
        /// Type of the moudule referenced
        /// </summary>
        public ModuleType moduleType;
        [JsonProperty("moduleType")]
        public ModuleType ModuleType
        {
            get
            {
                return moduleType;
            }
            set
            {
                Set(() => ModuleType, ref moduleType, value);
            }
        }

        /// <summary>
        /// Id of the module referenced
        /// </summary>
        public String moduleId;
        [JsonProperty("moduleId")]
        public String ModuleId
        {
            get
            {
                return moduleId;
            }
            set
            {
                Set(() => ModuleId, ref moduleId, value);
            }
        }

        /// <summary>
        /// When it is added to the Hub
        /// </summary>
        private DateTime addedOn;
        [JsonProperty("addedOn")]
        public DateTime AddedOn
        {
            get
            {
                return addedOn;
            }
            set
            {
                Set(() => AddedOn, ref addedOn, value);
            }
        }

        public MyModule()
        {

        }

        public MyModule(String ID, ModuleType ModuleType, String ModuleId)
        {
            this.Id = ID;
            this.ModuleType = ModuleType;
            this.ModuleId = ModuleId;
            this.AddedOn = DateTime.UtcNow;
        }

        public MyModule(String ID, ModuleType ModuleType, String ModuleId, DateTime AddedOn)
        {
            this.Id = ID;
            this.ModuleType = ModuleType;
            this.ModuleId = ModuleId;
            this.AddedOn = AddedOn;
        }

    }
}
