﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WARAN.Hub.Master.Model
{
    public class DB
    {

        public const String DB_PATH = "db.sqlite";

        public static SQLite.Net.SQLiteConnection Connection;

        static DB(){
            try
            {
                Connection = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, DB_PATH));
                Connection.CreateTable<Modules.PlantWaterer>(SQLite.Net.Interop.CreateFlags.None);
                Connection.CreateTable<Modules.TemperatureMonitor>(SQLite.Net.Interop.CreateFlags.None);
                Connection.CreateTable<MyModule>(SQLite.Net.Interop.CreateFlags.None);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public static T Get<T>(String Id)
        {
            if(typeof(T) == typeof(Modules.PlantWaterer))
            {
                return (T)(object)Connection.Table<Modules.PlantWaterer>().FirstOrDefault(p => p.Id == Id);
            }
            if (typeof(T) == typeof(Modules.TemperatureMonitor))
            {
                return (T)(object)Connection.Table<Modules.TemperatureMonitor>().FirstOrDefault(p => p.Id == Id);
            }
            return default(T);
        }

        public static T Save<T>(T Data)
        {
            if(Connection.Update(Data, typeof(T)) == 0)
            {
                Connection.Insert(Data, typeof(T));
            }
            return Data;
        }
        
        public static void Delete<T>(T Data)
        {   
            Connection.Delete(Data);
        }

        /// <summary>
        /// Get the mymodule list
        /// </summary>
        /// <returns></returns>
        public static List<MyModule> GetMyModules()
        {
            List<MyModule> mymodules = new List<MyModule>();
            mymodules.AddRange(Connection.Table<MyModule>().ToList());
            return mymodules;
        }

        /// <summary>
        /// Get the list of all modules from DB. Fetch from all tables and group it together and return
        /// </summary>
        /// <returns></returns>
        public static List<Modules.BaseModule> GetModules()
        {
            List<Modules.BaseModule> modules = new List<Modules.BaseModule>();
            modules.AddRange(Connection.Table<Modules.PlantWaterer>().ToList());
            modules.AddRange(Connection.Table<Modules.TemperatureMonitor>().ToList());
            return modules;
        }

    }
}
