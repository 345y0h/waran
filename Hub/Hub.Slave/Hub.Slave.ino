#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <Wire.h>

#define CE_PIN 7
#define CSN_PIN 8

RF24 radio(CE_PIN, CSN_PIN); //RF radio object to send and receive data via nRF24L01+

#define SLAVE_ADDRESS 0x40 //I2C Slave address

#define COMMAND_SIZE 32

byte hubAddress[10] = "WARANADDR"; //Radio address of the hub

byte counter = 1;
char moduleResponse[32];

void setup() {
    Serial.begin(57600);
    radio.begin();
    radio.enableAckPayload();                     // Allow optional ack payloads
    radio.enableDynamicPayloads();                // Ack payloads are dynamic payloads
    radio.openReadingPipe(1, hubAddress);
    radio.startListening();                       // Start listening  
    radio.writeAckPayload(1,&counter,1);          // Pre-load an ack-paylod into the FIFO buffer for pipe 1
    Wire.begin(SLAVE_ADDRESS);
    Wire.onReceive(receiveCommand);               //Call receiveCommand whenever a data is received from RPI2
    Wire.onRequest(respondCommand);               //Call respondCommand whenever a RPI2 requests data
}

void loop() {
}

String moduleAddress = "";
String moduleRequest = "";
String moduleRequestData = "";
String oldModuleAddress = "";

void receiveCommand(int numBytes){
    radio.stopListening(); //Stop the radio is its already listening
    String command = readCommand(numBytes); //Read data sent from RPI2
    splitCommand(command); //Split the command into address and command
    if(!oldModuleAddress.equals(moduleAddress)){ //Check if the previously received command not for the same module
        byte moduleAddressBytes[10];
        moduleAddress.getBytes(moduleAddressBytes, 10);
        radio.openWritingPipe(moduleAddressBytes); //Open the write pipe in radio based on the address received
        oldModuleAddress = moduleAddress;
    }
    char moduleRequestChars[moduleRequest.length() + 1];
    moduleRequest.toCharArray(moduleRequestChars, moduleRequest.length() + 1);
    Serial.println(moduleRequestChars);
    if(!radio.write(&moduleRequestChars, COMMAND_SIZE)){ //Send the command to the module based onthe address received
        Serial.println("FAILED");
        strncpy(moduleResponse, "FAILED", COMMAND_SIZE);
    }
    else{
        radio.startListening(); //After successfully send the command start listening for the response fomr module
        while(!radio.available()){} //Wait until response is received
        if(radio.available()){
            radio.read(&moduleResponse, COMMAND_SIZE); //Read the received data and assign it to moduleResponse
            Serial.println(moduleResponse);
        }
    }
}

void respondCommand(){
    Wire.write(moduleResponse); //Send the reponse got from the module (moduleResponse) back to RPI2
    for(int i = 0; i < sizeof(moduleResponse); i++){ //Reset moduleResponse
        moduleResponse[i] = 255;
    }
}

String readCommand(int commandSize){
    String command = "";
    int i = 0;
    for(; Wire.available(); i++){
        char c = Wire.read();
        command = command + c;
    }
    return command;
}

void splitCommand(String command){
    //The command will be of the format <moduleAddress>$<moduleRequest> or <moduleAddress>$<moduleRequest>$<moduleRequestData>
    moduleAddress = command.substring(0, command.indexOf('$'));
    moduleRequest = command.substring(command.indexOf('$') + 1);
    if(moduleRequest.indexOf('$') >= 0){
        moduleRequestData = moduleRequest.substring(moduleRequest.indexOf('$') + 1);
        moduleRequest = moduleRequest.substring(0, moduleRequest.indexOf('$'));
    }
    else{
        moduleRequestData = "";
    }
}
