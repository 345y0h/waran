﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;
using System.Linq;

namespace WARAN.Hub.Mobile.Converters
{
    public class ModuleIdToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var locator = App.Current.Resources["Locator"] as ViewModel.ViewModelLocator;
            return locator.Main.Modules.Where(p => p.Id == value.ToString()).FirstOrDefault().Name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}
