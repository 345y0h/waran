﻿using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WARAN.Hub.Mobile.Model.Modules
{
    public class BaseModule : ObservableObject
    {
        private String id = String.Empty;
        [JsonProperty("id")]
        public String Id
        {
            get
            {
                return id;
            }
            set
            {
                Set(() => Id, ref id, value);
            }
        }

        private String name = String.Empty;
        [JsonProperty("name")]
        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                Set(() => Name, ref name, value);
            }
        }

        private String address = String.Empty;
        [JsonProperty("address")]
        public String Address
        {
            get
            {
                return address;
            }
            set
            {
                Set(() => Address, ref address, value);
            }
        }

        private ModuleType type = ModuleType.None;
        [JsonProperty("type")]
        public ModuleType Type
        {
            get
            {
                return type;
            }
            set
            {
                Set(() => Type, ref type, value);
            }
        }

        private DateTime addedOn = DateTime.MinValue;
        [JsonProperty("addedOn")]
        public DateTime AddedOn
        {
            get
            {
                return addedOn;
            }
            set
            {
                Set(() => AddedOn, ref addedOn, value);
            }
        }

        public BaseModule(ModuleType Type)
        {
            this.Type = Type;
        }

        public BaseModule(String Id, String Name, ModuleType Type)
        {
            this.Id = Id;
            this.Name = Name;
            this.Type = Type;
            this.AddedOn = DateTime.Now;
        }
    }
}
