﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WARAN.Hub.Mobile.Model.Modules
{
    public enum ModuleType
    {
        None,
        PlantWaterer,
        TemperatureMonitor,
        IntrusionDetector,
        GasLeakDetector
    }
}
