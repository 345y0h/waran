﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WARAN.Hub.Mobile.Model.Modules
{
    [DataContract(Name = "plantwaterer")]
    public class PlantWaterer : BaseModule
    {
        private int waterLevel = 0;
        [JsonProperty("waterLevel")]
        public int WaterLevel
        {
            get {
                return waterLevel;
            }
            set
            {
                Set(() => WaterLevel, ref waterLevel, value);
            }
        }

        private double temperature = 0;
        [JsonProperty("temperature")]
        public double Temperature
        {
            get
            {
                return temperature;
            }
            set
            {
                Set(() => Temperature, ref temperature, value);
            }
        }
        
        public double TemperatureF
        {
            get
            {
                return Common.Utils.Temperature.CelsiusToFahrenheit(Temperature);
            }
        }

        private double humidity = 0;
        [JsonProperty("humidity")]
        public double Humidity
        {
            get
            {
                return humidity;
            }
            set
            {
                Set(() => Humidity, ref humidity, value);
            }
        }

        private bool autoWater = false;
        [JsonProperty("autoWater")]
        public bool AutoWater
        {
            get
            {
                return autoWater;
            }
            set
            {
                Set(() => AutoWater, ref autoWater, value);
            }
        }

        private bool pumpRunning = false;
        [JsonProperty("pumpRunning")]
        public bool PumpRunning
        {
            get
            {
                return pumpRunning;
            }
            set
            {
                Set(() => PumpRunning, ref pumpRunning, value);
            }
        }

        public PlantWaterer(): base(ModuleType.PlantWaterer)
        {
            
        }
        public PlantWaterer(String Id, String Name): base(Id, Name, ModuleType.PlantWaterer)
        {
        }
    }
}
