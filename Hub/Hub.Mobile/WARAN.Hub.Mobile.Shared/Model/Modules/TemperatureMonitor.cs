﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WARAN.Hub.Mobile.Model.Modules
{
    [DataContract(Name = "temperaturemonitor")]
    public class TemperatureMonitor : BaseModule
    {

        private double temperature = 0;
        [JsonProperty("temperature")]
        public double Temperature
        {
            get
            {
                return temperature;
            }
            set
            {
                Set(() => Temperature, ref temperature, value);
            }
        }
        
        public double TemperatureF
        {
            get
            {
                return Common.Utils.Temperature.CelsiusToFahrenheit(Temperature);
            }
        }

        private double humidity = 0;
        [JsonProperty("humidity")]
        public double Humidity
        {
            get
            {
                return humidity;
            }
            set
            {
                Set(() => Humidity, ref humidity, value);
            }
        }

        private double feelsLike = 0;
        [JsonProperty("feelsLike")]
        public double FeelsLike
        {
            get
            {
                return feelsLike;
            }
            set
            {
                Set(() => FeelsLike, ref feelsLike, value);
            }
        }

        private bool thermoRunning = false;
        [JsonProperty("thermoRunning")]
        public bool ThermoRunning
        {
            get
            {
                return thermoRunning;
            }
            set
            {
                Set(() => ThermoRunning, ref thermoRunning, value);
            }
        }

        public TemperatureMonitor(): base(ModuleType.PlantWaterer)
        {

        }

        public TemperatureMonitor(String Id, String Name): base(Id, Name, ModuleType.PlantWaterer)
        {
        }
    }
}
