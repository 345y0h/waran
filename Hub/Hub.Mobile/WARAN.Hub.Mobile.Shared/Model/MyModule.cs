﻿using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WARAN.Hub.Mobile.Model.Modules;

namespace WARAN.Hub.Mobile.Model
{
    [DataContract(Name = "mymodules")]
    public class MyModule : ObservableObject
    {
        private String id;
        [JsonProperty("id")]
        public String Id
        {
            get
            {
                return id;
            }
            set
            {
                Set(() => Id, ref id, value);
            }
        }

        public ModuleType moduleType;
        [JsonProperty("moduleType")]
        public ModuleType ModuleType
        {
            get
            {
                return moduleType;
            }
            set
            {
                Set(() => ModuleType, ref moduleType, value);
            }
        }

        public String moduleId;
        [JsonProperty("moduleId")]
        public String ModuleId
        {
            get
            {
                return moduleId;
            }
            set
            {
                Set(() => ModuleId, ref moduleId, value);
            }
        }

        private DateTime addedOn;
        [JsonProperty("addedOn")]
        public DateTime AddedOn
        {
            get
            {
                return addedOn;
            }
            set
            {
                Set(() => AddedOn, ref addedOn, value);
            }
        }

        public MyModule()
        {

        }

        public MyModule(String ID, ModuleType ModuleType, String ModuleId)
        {
            this.Id = ID;
            this.ModuleType = ModuleType;
            this.ModuleId = ModuleId;
            this.AddedOn = DateTime.UtcNow;
        }

        public MyModule(String ID, ModuleType ModuleType, String ModuleId, DateTime AddedOn)
        {
            this.Id = ID;
            this.ModuleType = ModuleType;
            this.ModuleId = ModuleId;
            this.AddedOn = AddedOn;
        }

    }
}
