﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using WARAN.Hub.Mobile.Model.Modules;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace WARAN.Hub.Mobile.Views.Controls
{
    public sealed partial class ModuleContainer : UserControl
    {

        public static readonly DependencyProperty ModuleIdProperty = DependencyProperty.Register("ModuleId", typeof(String), typeof(ModuleContainer), new PropertyMetadata(null));

        ViewModel.ViewModelLocator locator;

        public String ModuleId
        {
            get
            {
                return (String)GetValue(ModuleIdProperty);
            }
            set
            {
                SetValue(ModuleIdProperty, value);
            }
        }

        public static readonly DependencyProperty ModuleTypeProperty = DependencyProperty.Register("ModuleType", typeof(ModuleType), typeof(ModuleContainer), new PropertyMetadata(null));

        public ModuleType ModuleType
        {
            get
            {
                if (GetValue(ModuleTypeProperty) == null)
                {
                    return Model.Modules.ModuleType.None;
                }
                return (ModuleType)GetValue(ModuleTypeProperty);
            }
            set
            {
                SetValue(ModuleTypeProperty, value);
            }
        }

        public ModuleContainer()
        {
            this.InitializeComponent();
            this.Loaded += ModuleContainer_Loaded;
        }

        private void ModuleContainer_Loaded(object sender, RoutedEventArgs e)
        {
            locator = App.Current.Resources["Locator"] as ViewModel.ViewModelLocator;
            switch (ModuleType)
            {
                case Model.Modules.ModuleType.PlantWaterer:
                    PlantWaterer plantWaterer = (PlantWaterer)locator.Main.Modules.FirstOrDefault(p => p.Id == ModuleId);
                    Container.Children.Add(new Controls.Modules.PlantWaterer(plantWaterer));
                    break;
                case Model.Modules.ModuleType.TemperatureMonitor:
                    TemperatureMonitor temperatureMonitor = (TemperatureMonitor)locator.Main.Modules.FirstOrDefault(p => p.Id == ModuleId);
                    Container.Children.Add(new Controls.Modules.TemperatureMonitor(temperatureMonitor));
                    break;
            }
        }
    }
}
