﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using WARAN.Hub.Mobile.ViewModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace WARAN.Hub.Mobile.Views.Controls.Modules
{
    public sealed partial class TemperatureMonitor : UserControl
    {

        public Model.Modules.TemperatureMonitor CurrentData = null;

        ViewModelLocator locator = null;

        public const String COMMAND_START_THERMO = "START_THERMO";
        public const String COMMAND_STOP_THERMO = "STOP_THERMO";

        public TemperatureMonitor()
        {
            this.InitializeComponent();
        }

        public TemperatureMonitor(Model.Modules.TemperatureMonitor Data)
        {
            this.DataContext = this.CurrentData = Data;
            this.InitializeComponent();
            this.Loaded += TemperatureMonitor_Loaded;
        }

        private void TemperatureMonitor_Loaded(object sender, RoutedEventArgs e)
        {
            locator = App.Current.Resources["Locator"] as ViewModelLocator;
        }

        private void ToggleThermo_Click(object sender, RoutedEventArgs e)
        {
            CurrentData.ThermoRunning = !CurrentData.ThermoRunning;
            Model.Cloud.Send<object>("temperaturemonitor", new { command = CurrentData.ThermoRunning ? COMMAND_START_THERMO : COMMAND_STOP_THERMO, address = CurrentData.Address });
        }
    }
}
