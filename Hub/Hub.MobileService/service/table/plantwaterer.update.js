function update(item, user, request) {
    request.execute();
    //Updating the data in cloud db and sending information to mobile app that data has been updated in backend via PubNub 
    var pubnub = require("pubnub")({
        ssl           : true,  // <- enable TLS Tunneling over TCP
        publish_key   : process.env.PUBNUB_PUB_KEY,
        subscribe_key : process.env.PUBNUB_SUB_KEY
    });
    pubnub.publish({ 
        channel   : process.env.PUBNUB_DATA_CHANNEL, //All data related information will go through data channel of pubnub
        message   : { method: "UPDATE", data: item },
        callback  : function(e) { console.log( "SUCCESS!", e ); },
        error     : function(e) { console.log( "FAILED! RETRY PUBLISH!", e ); }
    });
    if(item.waterLevel < 20){ //Sending push notification when water level goes below 20%
        var payload = '<?xml version="1.0" encoding="utf-8"?><toast><visual><binding template="ToastText01">' +
        '<text id="1">' + item.name + ' is running out of water</text></binding></visual></toast>';
        push.wns.send(null,
            payload,
            'wns/toast', {
            success: function (pushResponse) {
                console.log("Sent push:", pushResponse);
            }
        });
    }
}