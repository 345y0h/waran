function del(id, user, request) {
    request.execute();
    var pubnub = require("pubnub")({
        ssl           : true,  // <- enable TLS Tunneling over TCP
        publish_key   : process.env.PUBNUB_PUB_KEY,
        subscribe_key : process.env.PUBNUB_SUB_KEY
    });
    pubnub.publish({ 
        channel   : process.env.PUBNUB_DATA_CHANNEL,
        message   : { method: "DELETE", data: id },
        callback  : function(e) { console.log( "SUCCESS!", e ); },
        error     : function(e) { console.log( "FAILED! RETRY PUBLISH!", e ); }
    });
}