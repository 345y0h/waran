var pubnub = require("pubnub")({
    ssl           : true,  // <- enable TLS Tunneling over TCP
    publish_key   : process.env.PUBNUB_PUB_KEY,
    subscribe_key : process.env.PUBNUB_SUB_KEY
});

//All commands mobile app for the plant waterer goes through this.
exports.post = function(request, response) {
    pubnub.publish({ 
        channel   : process.env.PUBNUB_ACTION_CHANNEL, //Action channel is the pubnub channel thorugh which the commands send mobile app are sent
        message   : { address: request.body.address, command: request.body.command },
        callback  : function(e) { console.log( "SUCCESS!", e ); },
        error     : function(e) { console.log( "FAILED! RETRY PUBLISH!", e ); }
    });
    response.send(statusCodes.OK, { success: true });
};

exports.get = function(request, response) {
    response.send(statusCodes.OK, { message : 'Hello World!' });
};