#include <SPI.h>
#include "DHT.h"
#include <EEPROM.h>

#include "nRF24L01.h"
#include "RF24.h"

#define COMMAND_SIZE 32

#define HYGROPIN A3
#define HYGROMAXTHRESHOLD 90
#define HYGROMINTHRESHOLD 40

#define DHTPIN 6
#define DHTTYPE DHT11

//All the commands possible in this module
#define COMMAND_READ_WATER_LEVEL "READ_WATER_LEVEL"
#define COMMAND_START_PUMP "START_PUMP"
#define COMMAND_STOP_PUMP "STOP_PUMP"
#define COMMAND_ENABLE_AUTO_WATER "ENABLE_AUTO_WATER"
#define COMMAND_DISABLE_AUTO_WATER "DISABLE_AUTO_WATER"

#define PUMP_PIN 5

#define CE_PIN 7
#define CSN_PIN 8

#define IS_AUTO_WATERING 0

DHT dht(DHTPIN, DHTTYPE);
RF24 radio(CE_PIN, CSN_PIN);

//Address of the current module.
//Make this unique for every module.
//This is what the hub uses to identify the module.
//This is address that is given in the add new module dialog.
//Also make sure the address is exactly 9 characters long
byte moduleAddress[10] = "PWPL46690";
byte hubAddress[10] = "WARANADDR"; //Radio address of the hub

int isWatering = 0;
int isAutoWatering = 0;
char command[32];

int temperature, humidity, soilMoisture;

byte pipeNo;

void setup() {
    Serial.begin(57600);
    
    radio.begin();
    radio.enableAckPayload();                     // Allow optional ack payloads
    radio.enableDynamicPayloads();                // Ack payloads are dynamic payloads

    radio.openWritingPipe(hubAddress);
    radio.openReadingPipe(1,moduleAddress);
    
    dht.begin();
    pinMode(PUMP_PIN, OUTPUT);
    
    isAutoWatering = EEPROM.read(IS_AUTO_WATERING);
    if(isAutoWatering != 0 && isAutoWatering !=1) isAutoWatering = 0;
    
    radio.startListening();
    readData();
}

void loop() {
    bool gotData = false;
    while(radio.available(&pipeNo)){ //Check if radio is available
        readData(); //Read current sensor data
        radio.read(&command, COMMAND_SIZE); //Read data sent from hub
        gotData = true;
        if(String(command).equals(COMMAND_READ_WATER_LEVEL)){
            sendResponse();
        }
        else if(String(command).equals(COMMAND_START_PUMP)){
            isWatering = 1;
            digitalWrite(PUMP_PIN, HIGH); //Start the pump
            sendResponse();
        }
        else if(String(command).equals(COMMAND_STOP_PUMP)){
            isWatering = 0;
            digitalWrite(PUMP_PIN, LOW); //Stop the pump
            sendResponse();
        }
        else if(String(command).equals(COMMAND_ENABLE_AUTO_WATER)){
            isAutoWatering = 1;
            EEPROM.write(IS_AUTO_WATERING, isAutoWatering); //Enabling automatic watering
            sendResponse();
        }
        else if(String(command).equals(COMMAND_DISABLE_AUTO_WATER)){
            isAutoWatering = 0;
            EEPROM.write(IS_AUTO_WATERING, isAutoWatering); //Disabling automatic watering
            isWatering = 0;
            digitalWrite(PUMP_PIN, LOW); //Stop the pump if running
            sendResponse();
        }
    }
    if(isAutoWatering == 1){ //If the auto watering is enabled start/stop pump basedon min/max threshold values
        if(soilMoisture < HYGROMINTHRESHOLD){
            isWatering = 1;
            digitalWrite(PUMP_PIN, HIGH);
        }
        else if(soilMoisture > HYGROMAXTHRESHOLD){
            isWatering = 0;
            digitalWrite(PUMP_PIN, LOW);
        }
    }
}

void sendResponse(){
    //Send response in the format <soilMoisture>|<temperature>|<humidity>|<isWatering>|<isAutoWatering>
    String response = String(soilMoisture) + "|" + String(temperature) + "|" + String(humidity) + "|" + String(isWatering) + "|" + String(isAutoWatering);
    char responseBuffer[COMMAND_SIZE];
    response.toCharArray(responseBuffer, response.length() + 1);
    Serial.println(responseBuffer);
    radio.writeAckPayload(pipeNo, &responseBuffer, COMMAND_SIZE);
    radio.stopListening();
    radio.write(&responseBuffer, COMMAND_SIZE); //Send the composed response back to hub
    radio.startListening();
}

//Read sensor data
void readData(){
    temperature = dht.readTemperature();
    humidity = dht.readHumidity();
    soilMoisture = analogRead(HYGROPIN);
    soilMoisture = constrain(soilMoisture, 60, 1023);
    soilMoisture = map(soilMoisture, 60, 1023, 100, 0); //Convert soil moisture level to percentage
}
